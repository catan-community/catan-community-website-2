const mongo = require('mongodb');

let url = "mongodb+srv://" + encodeURIComponent(process.env["MONGODB_USER"]) +
    ":" + encodeURIComponent(process.env.MONGODB_PASSWORD) +
    "@ccdevcluster0.ndot3.mongodb.net/" + encodeURIComponent(process.env["MONGODB_DATABASE"]) + "?retryWrites=true&w=majority";

let conn = new mongo.MongoClient(
    url,
    {
        useUnifiedTopology: true
    });

const connection_promise = conn.connect()

module.exports = async (req, res) => {
    await connection_promise;

    const database = conn.db(process.env["MONGODB_DATABASE"]);
    res.setHeader(
        'cache-control', 'public, max-age=3600, max-stale=7200' //One hour
    )
    res.json({
        users: (
            await database.collection('users').find({}
            ).project({
                    "lp": 1,
                    "matches": 1,
                    "display_name": 1,
                    "normalized_wins": 1,
                    "tournaments": 1,
                    "_id": 0
                }

            ).sort({'lp': -1}).toArray()
        ),
        tournaments: (await database.collection('tournaments').find({},
            {
                fields: {"_id": 0}
            }
        ).toArray()).map(
            (i) => i
        )
    })
}
