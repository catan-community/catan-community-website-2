import { createApp, h } from 'vue';
import App from './App.vue'
import VueMarkdownIt from 'vue3-markdown-it';
import router from './router'
import "bootstrap/dist/css/bootstrap.min.css"
import "bootstrap-vue/dist/bootstrap-vue.css"

createApp({
  render: () => h(App),

}).use(router).use(VueMarkdownIt).mount('#app')
