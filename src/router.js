import { createRouter, createWebHistory } from 'vue-router'
import Home from './views/HomeView.vue'

const routes = [
    {
        path: '/',
        name: 'home',
        component: Home
    },
    {
        path: '/about',
        name: 'about',
        component: () => import(/* webpackChunkName: "about" */ './views/AboutView.vue')
    },
    {
        path: '/leaderboard',
        name: 'leaderboard',
        component: () => import(/* webpackChunkName: "leaderboard" */ './views/LeaderboardView.vue')
    },
    {
        path: '/league-points',
        name: 'league-points',
        component: () => import(/* webpackChunkName: "league-points" */ './views/LeaguePointsView.vue')
    },
    {
        path: '/bot-docs',
        name: 'bot-docs',
        component: () => import(/* webpackChunkName: "bot-docs" */ './views/BotDocsView.vue')
    },
    {
        path: '/contact',
        name: 'contact',
        component: () => import(/* webpackChunkName: "contact" */ './views/ContactView.vue')
    },
    {
        path: '/privacy-policy',
        name: 'privacy-policy',
        component: () => import(/* webpackChunkName: "privacy-policy" */ './views/PrivacyPolicyView.vue')
    },    
    {
        path: '/current-tournament',
        name: 'current-tournament',
        component: () => import(/* webpackChunkName: "current-tournament" */ './views/CurrentTournamentView.vue')
    },
    {
        path: '/open-tournament-registration',
        name: 'open-tournament-registration',
        component: () => import(/* webpackChunkName: "open-tournament-registration" */ './views/OpenTournamentRegistrationView.vue')
    },
    {
        path: '/cash-tournament-registration',
        name: 'cash-tournament-registration',
        component: () => import(/* webpackChunkName: "cash-tournament-registration" */ './views/CashTournamentRegistrationView.vue')
    }
]
export default createRouter({
    history: createWebHistory(),
    routes
})
