const fetch = require('sync-fetch');

module.exports = {
    configureWebpack: {
        module: {
            rules: [{
                test: /\.md$/,
                use: 'raw-loader'
            }]
        }
    },
    chainWebpack: config => {
        const readme = fetch("https://gitlab.com/mousetail/catan-community-discord-bot/-/raw/master/readme.md");
        const readme_text = readme.text();

        config.plugin('define').tap(args => {


            args[0].README = JSON.stringify(readme_text);
            args[0].__VUE_PROD_DEVTOOLS__ = 'false';
            args[0].__VUE_OPTIONS_API__ = 'true';
            return args
        })
    }
}